import { shallowMount } from '@vue/test-utils';
import moxios from 'moxios';

import App from './../../src/App';
import ProductView from '@/components/ProductView';
import {mutations} from './../../src/store/mutations';

let wrapper;
let products = [
  {
    _id: '5edb842688e2945723e60673',
    name: 'Aerodynamic Concrete Keyboard',
    supplier: {
      companyName: 'Balistreri LLC',
      contactName: 'Marchelle Sawayn MD',
      contactTitle: 'Product Markets Analyst',
      address: '232 Ritchie Mews',
      city: 'Emmerichchester',
      region: 'Port Kaseyland',
      postalCode: '89353-8936',
      country: 'Vietnam',
      fax: '',
      homePage: 'www.leanne-jerde.com',
    },
    category: {
      name: 'Scorpion Strike',
      description:
        'Vitae magni illum omnis impedit. Quia occaecati minima voluptate. Quaerat consequuntur a nulla beatae sapiente qui qui. Unde mollitia nisi non nemo voluptatem recusandae. Ut velit quae et. In et dignissimos voluptate consequatur.',
      picture: 'https://s3.amazonaws.com/uifaces/faces/twitter/ssbb_me/128.jpg',
    },
    quantityPerUnit: '4',
    unitPrice: '60,03',
    unitsInStock: '24',
    unitsOnOrder: '0',
    reorderLevel: '3',
    discontinued: false,
    _class: 'co.za.eblocks.warehousemanager.model.Product',
  },
  {
    _id: '5edb842688e2945723e60674',
    name: 'Enormous Leather Bottle',
    supplier: {
      companyName: 'Barrows Inc',
      contactName: 'Miss Bessie Hagenes',
      contactTitle: 'Internal Identity Engineer',
      address: '28328 Abe Lock',
      city: 'Tammouth',
      region: 'Kierastad',
      postalCode: '52162-9836',
      country: 'Portugal',
      fax: '',
      homePage: 'www.makeda-heathcote.io',
    },
    category: {
      name: 'Bat Fist',
      description:
        'Aspernatur odio sunt delectus culpa. Recusandae ut assumenda quaerat deleniti tempore. Velit nesciunt est reprehenderit laborum id qui natus. Vel quis aut. Eius fuga non eligendi fugiat id. Necessitatibus eligendi aliquam autem. Ut tenetur incidunt illum enim dolores ab.',
      picture:
        'https://s3.amazonaws.com/uifaces/faces/twitter/antonyryndya/128.jpg',
    },
    quantityPerUnit: '0',
    unitPrice: '62,88',
    unitsInStock: '24',
    unitsOnOrder: '3',
    reorderLevel: '2',
    discontinued: false,
    _class: 'co.za.eblocks.warehousemanager.model.Product',
  },
  {
    _id: '5edb842688e2945723e60675',
    name: 'Aerodynamic Rubber Wallet',
    supplier: {
      companyName: 'Abshire-McCullough',
      contactName: "Delana O'Connell",
      contactTitle: 'Future Factors Strategist',
      address: '508 Pamila Keys',
      city: 'Jakubowskifort',
      region: 'Mickeyberg',
      postalCode: '63815-8479',
      country: 'Northern Mariana Islands',
      fax: '',
      homePage: 'www.malcom-bartell.io',
    },
    category: {
      name: 'Ultra Beetle Brain',
      description:
        'Eos magnam adipisci quidem labore sed velit. Culpa in et. Ab in dolorum non qui optio dolorem et. Hic fugit sit sapiente aliquam quia. Quam quam quaerat commodi quisquam ut minima.',
      picture:
        'https://s3.amazonaws.com/uifaces/faces/twitter/justinrgraham/128.jpg',
    },
    quantityPerUnit: '9',
    unitPrice: '80,03',
    unitsInStock: '2',
    unitsOnOrder: '3',
    reorderLevel: '1',
    discontinued: false,
    _class: 'co.za.eblocks.warehousemanager.model.Product',
  },
];

beforeEach(() => {
  moxios.install();
  wrapper = shallowMount(App);
  moxios.stubRequest('http://localhost:8080/api/v1/products', {
    status: 200,
    response: products,
  });
});

afterEach(() => {
  moxios.uninstall();
  wrapper.destroy();
});

describe('App', () => {
  it('should have a title', () => {
    const title = wrapper.findAll('.title');
    expect(title.length).toEqual(1);
  });

  it('should have ProductView component as one of its elements', () => {
    moxios.wait(() => {
      expect(wrapper.findComponent(ProductView).exists()).toBe(true);
    });
  });

  it('should fetch data from backend on mount', () => {
    let productsState = wrapper.vm.products;
    moxios.wait(() => {
      expect(productsState).toNotEqual(wrapper.vm.products);
      expect(wrapper.vm.products.length).toBe(3);
    });
  });

  it('should display a list of products', () => {
    moxios.wait(() => {
      const productItems = wrapper.findAll('.product');
      expect(productItems.length).toBe(products.length);
    });
  });

  it('should have a search input', () => {
    moxios.wait(() => {
      expect(wrapper.find('input.search').length).toBe(1);
    });
  });

  it('should have a select with options', () => {
    moxios.wait(() => {
      expect(wrapper.find('select').length).toBe(1);
      expect(wrapper.find('option').length).toBe(3);
      expect(wrapper.find('option')[0]).toBe(
        '<option value="name">name</option>'
      );
    });
  });

  it('should have a search button', () => {
    moxios.wait(() => {
      const button = wrapper.find('button.button.is-primary');
      expect(button.length).toBe(1);
      expect(button.textContent).toBe('search');
    });
  });

  it('should update the keyword property', () => {
    moxios.wait(() => {
      const input = wrapper.find('input.search');
      input.setValue('Aerodynamic');
      expect(wrapper.keyword).toBe('Aerodynamic');
    });
  });

  it('should constantly poll the backend for data', () => {

  });
});
