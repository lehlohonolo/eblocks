// Import `shallowMount` from Vue Test Utils and the component being tested
import { shallowMount } from '@vue/test-utils';
import ProductView from '@/components/ProductView';

let wrapper = null;
const prod = {
  name: 'Test Product',
  category: {
    name: 'Test Category',
    description: 'test',
  },
  supplier: {
    companyName: 'Test Company',
  },
};

beforeEach(() => {
  wrapper = shallowMount(ProductView, {
    propsData: {
      product: prod,
    },
  });
});

afterEach(() => {
  wrapper.destroy();
});

describe('ProductView', () => {
  it('should render', () => {
    expect(wrapper.exists()).toBe(true);
  });

  it('should render product details', () => {
    const name = wrapper.findAll('h1.name');
    const category = wrapper.findAll('span.category');
    const supplier = wrapper.findAll('span.supplier');

    expect(name.length).toBe(1);
    expect(category.length).toBe(1);
    expect(supplier.length).toBe(1);

    expect(wrapper.props().product).toEqual(prod);
  });
});
