package co.za.eblocks.warehousemanager.utils;

import org.modelmapper.ModelMapper;

public class DTOModelMapper {
    private ModelMapper modelMapper = new ModelMapper();

    public <T> T mapToType(final Object object, final Class<T> klass) {
        return modelMapper.map(object, klass);
    }
}