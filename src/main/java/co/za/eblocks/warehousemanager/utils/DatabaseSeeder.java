package co.za.eblocks.warehousemanager.utils;

import com.github.javafaker.Faker;

import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import co.za.eblocks.warehousemanager.model.Category;
import co.za.eblocks.warehousemanager.model.Product;
import co.za.eblocks.warehousemanager.model.Supplier;
import co.za.eblocks.warehousemanager.model.dto.CreateProductDTO;
import co.za.eblocks.warehousemanager.repository.ProductRepository;

@Component
public class DatabaseSeeder implements CommandLineRunner {

    private Faker faker = new Faker();
    private ModelMapper modelMapper = new ModelMapper();

    private ProductRepository productRepository;

    public DatabaseSeeder(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        dropAllProducts();
        generateProducts();
    }

    private void dropAllProducts() {
        productRepository.deleteAll();
    }

    private void generateProducts() {
        for (int i = 0; i < 15; i++) {
            Category category = generateCategory();
            Supplier supplier = generateSupplier();
            Product product = generateNewProduct(faker.commerce().productName(), supplier, category);
            productRepository.save(product);
        }
    }

    private Category generateCategory() {
        return new Category(faker.superhero().name(), faker.lorem().paragraph(5), faker.avatar().image());
    }

    private Supplier generateSupplier() {
        return new Supplier(faker.company().name(), faker.name().fullName(), faker.name().title(),
                faker.address().streetAddress(), faker.address().city(), faker.address().cityName(),
                faker.address().zipCode(), faker.address().country(), "", faker.internet().url());
    }

    private Product generateNewProduct(String name, Supplier supplier, Category category) {
        CreateProductDTO product = new CreateProductDTO(name, supplier, category,
                String.format("%s", (int) (Math.random() * 15)), faker.commerce().price(),
                String.format("%s", (int) (Math.random() * 25)), String.format("%s", (int) (Math.random() * 5)),
                String.format("%s", (int) (Math.random() * 4)), false);
        Product mappedtoProduct = maptoType(product, Product.class);
        mappedtoProduct.setSupplier(supplier);
        mappedtoProduct.setCategory(category);
        return mappedtoProduct;
    }

    protected <T> T maptoType(final Object object, final Class<T> klass) {
        return modelMapper.map(object, klass);
    }
}