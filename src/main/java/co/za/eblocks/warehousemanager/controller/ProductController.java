package co.za.eblocks.warehousemanager.controller;

import co.za.eblocks.warehousemanager.model.Product;
import co.za.eblocks.warehousemanager.model.dto.CreateProductDTO;
import co.za.eblocks.warehousemanager.model.dto.ProductsDTO;
import co.za.eblocks.warehousemanager.repository.ProductRepository;
import co.za.eblocks.warehousemanager.utils.DTOModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    private DTOModelMapper modelMapper = new DTOModelMapper();

    Logger logger = LoggerFactory.getLogger(ProductController.class);

    public ProductController(ProductRepository productRepository) {
        logger.info("Activating PostsController");
        this.productRepository = productRepository;
    }

    @PostMapping()
    public ResponseEntity<Product> create(@RequestBody CreateProductDTO product) {
        try {
            Product _product = modelMapper.mapToType(product, Product.class);
            return new ResponseEntity<>(productRepository.save(_product), HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error(String.format("Oops! An error has occured: error: [%s],  stacktrace: [%s]", e.getMessage(),
                    Arrays.toString(e.getStackTrace())));
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @GetMapping()
    public ResponseEntity<ProductsDTO> all(@RequestParam(required = false) String keyword,
            @RequestParam(required = false) String supplier, @RequestParam(required = false) String category) {
        try {
            List<Product> products = new ArrayList<Product>();

            if (keyword != null) {
                keyword = decodeValue(keyword);
                products.addAll(productRepository.findByNameContaining(keyword));
            } else if (supplier != null)
                products.addAll(productRepository.findBySupplierCompanyName(supplier));
            else if (category != null)
                products.addAll(productRepository.findByCategoryName(category));
            else
                products.addAll(productRepository.findAll());

            if (products.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            ProductsDTO productsDTO = new ProductsDTO();
            productsDTO.setCount(products.size());
            productsDTO.setProducts(products);

            return new ResponseEntity<>(productsDTO, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(
                    String.format("Oops! An error occurred while trying to retrieve products: [%s]", e.getMessage()));
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public static String decodeValue(String value) {
        try {
            return URLDecoder.decode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }
}