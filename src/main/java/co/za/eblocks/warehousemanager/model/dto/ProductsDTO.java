package co.za.eblocks.warehousemanager.model.dto;

import co.za.eblocks.warehousemanager.model.Product;
import lombok.Data;

import java.util.List;

@Data
public class ProductsDTO {
    private int count;
    private List<Product> products;
}
