package co.za.eblocks.warehousemanager.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import co.za.eblocks.warehousemanager.model.Category;
import co.za.eblocks.warehousemanager.model.Supplier;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateProductDTO {
    private String name;

    private Supplier supplier;

    private Category category;

    private String quantityPerUnit;

    private String unitPrice;

    private String unitsInStock;

    private String unitsOnOrder;

    private String reorderLevel;

    private boolean discontinued;
}