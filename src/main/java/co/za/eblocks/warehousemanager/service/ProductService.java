package co.za.eblocks.warehousemanager.service;

import co.za.eblocks.warehousemanager.exceptions.ProductNotFoundException;
import co.za.eblocks.warehousemanager.model.Product;
import co.za.eblocks.warehousemanager.repository.ProductRepository;

import java.util.List;

public class ProductService {
    ProductRepository productRepository;
    ProductService(ProductRepository productRepository){
        this.productRepository = productRepository;
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public List<Product> findByNameContaining(String keyword) {
        return productRepository.findByNameContaining(keyword);
    }

    public List<Product> findByCategoryName(String name) throws ProductNotFoundException {
        if(name.isEmpty()) throw new ProductNotFoundException();

        List<Product> products = productRepository.findByCategoryName(name);

        if(products.isEmpty()) throw new ProductNotFoundException();
        return products;
    }

    public List<Product> findBySupplierCompanyName(String companyName) {
        return productRepository.findBySupplierCompanyName(companyName);
    }
}
