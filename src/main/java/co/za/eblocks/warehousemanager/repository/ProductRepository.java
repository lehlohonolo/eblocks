package co.za.eblocks.warehousemanager.repository;

import co.za.eblocks.warehousemanager.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends MongoRepository<Product, String> {
    @Override
    List<Product> findAll();

    List<Product> findByNameContaining(String name);

    List<Product> findByCategoryName(String category);

    List<Product> findBySupplierCompanyName(String supplier);
}