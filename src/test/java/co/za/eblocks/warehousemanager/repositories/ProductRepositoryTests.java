package co.za.eblocks.warehousemanager.repositories;

import co.za.eblocks.warehousemanager.ApplicationTests;
import co.za.eblocks.warehousemanager.model.Category;
import co.za.eblocks.warehousemanager.model.Product;
import co.za.eblocks.warehousemanager.model.Supplier;
import co.za.eblocks.warehousemanager.repository.ProductRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@DataMongoTest
public class ProductRepositoryTests extends ApplicationTests {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    private Supplier supplier;
    private Category category;

    @BeforeEach
    public void setUp() {
        super.setUp();
        mongoTemplate.createCollection("products");
        supplier = createTestSupplier("Test Supplier");
        category = createTestCategory("Test Category");
    }

    @AfterEach
    public void tearDown() {
        supplier = null;
        category = null;
        // clear data after each run
        mongoTemplate.dropCollection("products");
    }

    @Test
    public void contextLoads() throws Exception {
        assertThat(productRepository).isNotNull();
    }

    @Test
    public void should_find_no_products_if_repository_is_empty() {
        Iterable<Product> products = productRepository.findAll();
        assertThat(products).isEmpty();
    }

    @Test
    public void should_store_a_new_product() {
        Product newProduct = createNewTestProduct("Test Product Create Product", supplier, category);
        Product mappedProduct = this.maptoType(newProduct, Product.class);
        // set supplier and category ids
        mappedProduct.setSupplier(supplier);
        mappedProduct.setCategory(category);

        // WHEN
        Product product = productRepository.save(mappedProduct);

        // THEN
        assertThat(product).hasFieldOrPropertyWithValue("name", "Test Product Create Product");
        assertThat(product).hasFieldOrPropertyWithValue("supplier", supplier);
        assertThat(product).hasFieldOrPropertyWithValue("category", category);
        assertThat(product).hasFieldOrPropertyWithValue("discontinued", false);
    }

    @Test
    public void should_find_all_products() {
        Product product1 = createNewTestProduct("Test Product 1", supplier, category);
        productRepository.save(product1);
        Product product2 = createNewTestProduct("Test Product 2", supplier, category);
        productRepository.save(product2);

        // WHEN
        Iterable<Product> products = productRepository.findAll();

        // THEN
        assertThat(products).hasSize(2).contains(product1, product2);
    }

    @Test
    public void should_find_product_by_id() {
        Product product = createNewTestProduct("Product to be retireved by id", supplier, category);
        productRepository.save(product);

        // WHEN
        Product retrievedProduct = productRepository.findById(product.getId()).get();

        // THEN
        assertThat(retrievedProduct).isEqualTo(product);
    }

    @Test
    public void should_filter_products_by_supplier_company_name() {

        Supplier supplier1 = createTestSupplier("RandomSupplierId-1");
        Supplier supplier2 = createTestSupplier("RandomSupplierId-2");
        Supplier supplier3 = createTestSupplier("RandomSupplierId-3");

        Product product = createNewTestProduct("name 1", supplier1, category);
        productRepository.save(product);
        Product product2 = createNewTestProduct("name 2", supplier1, category);
        productRepository.save(product2);
        Product product3 = createNewTestProduct("name 3", supplier1, category);
        productRepository.save(product3);
        Product product4 = createNewTestProduct("name 4", supplier2, category);
        productRepository.save(product4);

        // WHEN
        Iterable<Product> supplierId_1_products = productRepository
                .findBySupplierCompanyName(supplier1.getCompanyName());
        Iterable<Product> supplierId_2_products = productRepository
                .findBySupplierCompanyName(supplier2.getCompanyName());
        Iterable<Product> supplierId_3_products = productRepository
                .findBySupplierCompanyName(supplier3.getCompanyName());

        // THEN
        assertThat(supplierId_1_products).hasSize(3).contains(product, product2, product3);
        assertThat(supplierId_2_products).hasSize(1).contains(product4);
        assertThat(supplierId_3_products).hasSize(0);
    }

    @Test
    public void should_filter_products_by_category_name() {

        Category category1 = createTestCategory("RandomCategoryName-1");
        Category category2 = createTestCategory("RandomCategoryName-2");
        Category category3 = createTestCategory("RandomCategoryName-3");

        Product product = createNewTestProduct("name 1", supplier, category1);
        productRepository.save(product);
        Product product2 = createNewTestProduct("name 2", supplier, category1);
        productRepository.save(product2);
        Product product3 = createNewTestProduct("name 3", supplier, category1);
        productRepository.save(product3);
        Product product4 = createNewTestProduct("name 4", supplier, category2);
        productRepository.save(product4);

        // WHEN
        Iterable<Product> category1_products = productRepository.findByCategoryName(category1.getName());
        Iterable<Product> category2_products = productRepository.findByCategoryName(category2.getName());
        Iterable<Product> category3_products = productRepository.findByCategoryName(category3.getName());

        // THEN
        assertThat(category1_products).hasSize(3).contains(product, product2, product3);
        assertThat(category2_products).hasSize(1).contains(product4);
        assertThat(category3_products).hasSize(0);
    }

    @Test
    public void should_find_product_by_name() {

        Product product = createNewTestProduct("name 1", supplier, category);
        productRepository.save(product);
        Product product2 = createNewTestProduct("A suitable name 2", supplier, category);
        productRepository.save(product2);
        Product product3 = createNewTestProduct("Test Man 3", supplier, category);
        productRepository.save(product3);

        // WHEN
        Iterable<Product> productsMatchinTextInName = productRepository.findByNameContaining("name");

        // THEN
        assertThat(productsMatchinTextInName).hasSize(2).contains(product, product2);
    }

    private Category createTestCategory(String categoryName) {
        return new Category(categoryName, "Test Contact Person", "avatar.jpg");
    }

    private Supplier createTestSupplier(String supplierName) {
        return new Supplier(supplierName, "Test Contact Person", "Production Manager", "1 Address Avenue", "Test City",
                "SomewhereInTheWorld", "1111", "ZA", "", "");
    }
}