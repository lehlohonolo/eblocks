package co.za.eblocks.warehousemanager;

import co.za.eblocks.warehousemanager.model.Category;
import co.za.eblocks.warehousemanager.model.Product;
import co.za.eblocks.warehousemanager.model.Supplier;
import co.za.eblocks.warehousemanager.model.dto.CreateProductDTO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import org.modelmapper.ModelMapper;

import java.io.IOException;

public abstract class ApplicationTests {
    protected Faker faker;

    ModelMapper modelMapper;

    protected void setUp() {
        modelMapper = new ModelMapper();
        faker = new Faker();
    }

    protected String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }

    protected <T> T mapFromJson(String json, Class<T> klass)
            throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, klass);
    }

    protected <T> T maptoType(final Object object, final Class<T> klass) {
        return modelMapper.map(object, klass);
    }

    protected Category createTestCategory() {
        return new Category(faker.superhero().name(), faker.lorem().paragraph(5), faker.avatar().image());
    }

    protected Supplier createTestSupplier() {
        return new Supplier(faker.company().name(), faker.name().fullName(), faker.name().title(),
                faker.address().streetAddress(), faker.address().city(), faker.address().cityName(),
                faker.address().zipCode(), faker.address().country(), "", faker.internet().url());
    }

    protected Product createNewTestProduct(String name, Supplier supplier, Category category) {
        CreateProductDTO product = new CreateProductDTO(name, supplier, category,
                String.format("%s", (int) (Math.random() * 15)), faker.commerce().price(),
                String.format("%s", (int) (Math.random() * 25)), String.format("%s", (int) (Math.random() * 5)),
                String.format("%s", (int) (Math.random() * 4)), false);
        Product mappedtoProduct = this.maptoType(product, Product.class);
        mappedtoProduct.setSupplier(supplier);
        mappedtoProduct.setCategory(category);
        return mappedtoProduct;
    }
}