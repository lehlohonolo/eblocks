package co.za.eblocks.warehousemanager.controller;

import co.za.eblocks.warehousemanager.ApplicationTests;
import co.za.eblocks.warehousemanager.model.Category;
import co.za.eblocks.warehousemanager.model.Product;
import co.za.eblocks.warehousemanager.model.Supplier;
import co.za.eblocks.warehousemanager.repository.ProductRepository;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = ProductController.class)
public class ProductControllerTests extends ApplicationTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;

    String url = "/api/v1/products";

    @Mock
    private ProductRepository productRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setUp() {
        super.setUp();
        faker = new Faker();
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void all_ShouldReturnAListOfProducts() throws Exception{
        Category c = this.createTestCategory();
        Supplier s = this.createTestSupplier();

        String name = String.format("Exclusive %s", faker.commerce().productName());
        Product inputProduct = createNewTestProduct(name, s, c);

        name = String.format("Exclusive %s", faker.commerce().productName());
        Product inputProduct2 = createNewTestProduct(name, s, c);

        name = String.format("Inclusive %s", faker.commerce().productName());
        Product inputProduct3 = createNewTestProduct(name, s, c);

        when(productRepository.findByNameContaining(anyString())).thenReturn(Arrays.asList(inputProduct, inputProduct2, inputProduct3));

        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
