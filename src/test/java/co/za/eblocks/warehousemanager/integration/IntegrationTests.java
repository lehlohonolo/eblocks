package co.za.eblocks.warehousemanager.integration;

import co.za.eblocks.warehousemanager.ApplicationTests;
import co.za.eblocks.warehousemanager.controller.ProductController;
import co.za.eblocks.warehousemanager.model.Category;
import co.za.eblocks.warehousemanager.model.Product;
import co.za.eblocks.warehousemanager.model.Supplier;
import co.za.eblocks.warehousemanager.model.dto.ProductsDTO;
import co.za.eblocks.warehousemanager.repository.ProductRepository;
import co.za.eblocks.warehousemanager.service.ProductService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTests extends ApplicationTests{

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductController controller;

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    private TestRestTemplate restTemplate;

    private final String uri = "/api/v1/products";

    @BeforeEach
    public void setUp() {
        super.setUp();
        faker = new Faker();
        if (mongoTemplate.collectionExists(Product.class)) {
            mongoTemplate.dropCollection(Product.class);
        }
        mongoTemplate.createCollection(Product.class);
    }

    @AfterEach
    public void tearDown(){
        if (!mongoTemplate.collectionExists(Product.class)) {
            return;
        }
        mongoTemplate.dropCollection(Product.class);
    }

    @Test
    public void contextLoads() throws Exception {
        assertThat(controller).isNotNull();
    }

    @Test
    public void should_create_a_product() throws Exception {
        Category c = createTestCategory();
        Supplier s = createTestSupplier();
        String name = faker.commerce().productName();
        Product inputProduct = createNewTestProduct(name, s, c);

        when(productRepository.save(any(Product.class))).thenReturn(inputProduct);

        String jsonInputCreateProductDTO = super.mapToJson(inputProduct);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request =
                new HttpEntity<String>(jsonInputCreateProductDTO, headers);

        ResponseEntity<Product> response = restTemplate.postForEntity(uri, request, Product.class);

        HttpStatus status = response.getStatusCode();
        assertEquals(HttpStatus.CREATED, status);
        Product content = response.getBody();
        assertThat(content.getName()).isEqualTo(name);
        assertThat(content.getCategory()).isEqualTo(c);
        assertThat(content.getSupplier()).isEqualTo(s);
    }

    @Test
    public void should_get_a_list_of_all_products() throws JsonProcessingException {
        Category c = createTestCategory();
        Supplier s = createTestSupplier();

        String name = faker.commerce().productName();
        Product testProduct = createNewTestProduct(name, s, c);

        // create test products
        when(productRepository.save(any(Product.class))).thenReturn(testProduct);

        String jsonInputCreateProduct1 = this.mapToJson(testProduct);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request =
                new HttpEntity<String>(jsonInputCreateProduct1, headers);
        ResponseEntity<Product> responseCreated = restTemplate.postForEntity(uri, request, Product.class);

        assertThat(responseCreated.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        // when
        ResponseEntity<ProductsDTO> response = restTemplate.getForEntity(uri, ProductsDTO.class);
        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isInstanceOf(ProductsDTO.class);
        ProductsDTO content = response.getBody();
        assertThat(content.getProducts()).hasSize(1);
    }
}