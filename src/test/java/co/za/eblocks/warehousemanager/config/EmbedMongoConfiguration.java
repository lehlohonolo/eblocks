package co.za.eblocks.warehousemanager.config;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

public class EmbedMongoConfiguration {
    private MongoClient mongoClient;

    @Autowired
    private Environment env;

    public EmbedMongoConfiguration(){
        initializeInstance();
    }

    public void initializeInstance(){
        String connectionString = getConnectionString();
        mongoClient = MongoClients.create(connectionString);
    }

    public void closeConnection()
    {
        mongoClient.close();
    }


    protected String getConnectionString() {
        return env.getProperty("spring.datasource.url") != null ? env.getProperty("spring.datasource.uri") : "mongodb://127.0.0.1:27017";
    }
}
