package co.za.eblocks.warehousemanager.service;

import co.za.eblocks.warehousemanager.ApplicationTests;
import co.za.eblocks.warehousemanager.exceptions.ProductNotFoundException;
import co.za.eblocks.warehousemanager.model.Category;
import co.za.eblocks.warehousemanager.model.Product;
import co.za.eblocks.warehousemanager.model.Supplier;
import co.za.eblocks.warehousemanager.repository.ProductRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

@MockitoSettings
@DataMongoTest
public class ProductServiceTests extends ApplicationTests {
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductService productService;

    @Autowired
    private MongoTemplate mongoTemplate;

    @BeforeEach
    public void setUp() {
        super.setUp();
        mongoTemplate.createCollection("products");
    }

    @AfterEach
    public void tearDown(){
        mongoTemplate.dropCollection("products");
    }

    @Test
    public void contextLoads() throws Exception {
        assertThat(productService).isNotNull();
    }

    @Test
    public void findAll_ReturnsAllProducts() {
        List<Product> testProducts = getTestProducts();

        given(productRepository.findAll()).willReturn(testProducts);

        List<Product> products = productService.findAll();
        assertThat(products).hasSize(5).containsAll(testProducts);
    }

    @Test
    public void findByNameContaining_ReturnsAllProductsMatchingName() {
        List<Product> products = getTestProducts();
        given(productRepository.findByNameContaining(anyString())).willReturn(Collections.singletonList(products.get(3)));
        assertThat(productService.findByNameContaining("Exclusive Product")).hasSize(1).contains(products.get(3));
    }

    @Test
    public void findByCategoryName_ReturnsAllProductsBelongingToSpecifiedCategoryName() throws Exception {
        List<Product> products = getTestProducts();
        given(productRepository.findByCategoryName(anyString())).willReturn(Collections.singletonList(products.get(3)));
        assertThat(productService.findByCategoryName(products.get(3).getCategory().getName())).hasSize(1).contains(products.get(3));
    }

    @Test
    public void findBySupplierName_ReturnsAllProductsBelongingToSpecifiedCategoryName() throws Exception {
        List<Product> products = getTestProducts();
        given(productRepository.findBySupplierCompanyName(anyString())).willReturn(Collections.singletonList(products.get(3)));
        assertThat(productService.findBySupplierCompanyName(products.get(3).getSupplier().getCompanyName())).hasSize(1).contains(products.get(3));
    }

    @Test
    public void findByProductName_ThrowsProductNotFoundException() throws Exception {
        Exception exception = assertThrows(ProductNotFoundException.class, () -> {
            productService.findByCategoryName("");
        });

        assertThatExceptionOfType(ProductNotFoundException.class);
    }

    protected List<Product> getTestProducts() {
        List<Product> products = new ArrayList<Product>();

        for (int i = 0; i < 5; i++) {
            Category c = this.createTestCategory();
            Supplier s = this.createTestSupplier();
            if (i == 3) {
                c.setName(String.format("Test Category: %s", c.getName()));
                s.setCompanyName(String.format("Test Supplier: %s", s.getCompanyName()));
            }
            String name = (i == 3) ? String.format("Exclusive Product: %s", faker.commerce().productName()) : faker.commerce().productName();
            products.add(this.createNewTestProduct(name, s, c));
        }

        return products;
    }
}
