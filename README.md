# Advantages of using a NoSQL Database
* Lesser Server Costs
* No fixed data models/No rigid database schemas
* The presence of an Integrated Caching Facility
* Cheap maintainance
* Easy to scale